function getCircleArea(radius) {
  if (radius <= 0 || typeof radius !== "number") {
    return undefined;
  } else {
    return 3.1416 * radius ** 2;
  }
}

module.exports = {
  getCircleArea: getCircleArea,
};
